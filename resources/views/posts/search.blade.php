@extends('layouts.app')

@section('title', 'Все посты')


@section('content')
    <a href="{{route('posts.create')}}" class="btn btn-success">создать Пост</a>
    @if (session()->get('success'))
        <div class="alert alert-success mt-3">
            {{session()->get('success')}}
        </div>
    @endif
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">ФИО</th>
            <th scope="col">Email</th>
            <th scope="col">Адрес контакта</th>
            <th scope="col">Изображение</th>
            <th scope="col">Номер телефона</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @if($posts)
            @foreach($posts as $post)
                <tr>
                    <th scope="row">{{$post->id}}</th>
                    <td>{{$post->fio}}</td>
                    <td>{{$post->email}}</td>
                    <td>{{$post->address}}</td>
                    <td><img src="{{ Storage::url($post->image) }}" height="75" width="75" alt="" /></td>
                    <td>{{$post->phone}}</td>
                    <td class="table-buttons">
                        <a href="{{route('posts.show',$post)}}" class="btn btn-success"><i class="fa fa-eye"></i></a>
                        <a href="{{route('posts.edit',$post)}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                        <form action="{{route('posts.destroy',$post)}}" method="post" >
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                        </form>

                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="3" class="text-danger">Записи не найдены.</td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection
