@extends('layouts.app')

@section('title', 'Добавить Пост')



@section('content')
    <div class="row">
        <div class="col-lg-6 mx-auto">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Error!</strong>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li></li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{route('posts.store')}}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="post-title">ФИО</label>
                    <input type="text" class="form-control" value="{{old('fio')}}" name="fio" id="post-fio" placeholder="ФИО">
                </div>
                <div class="form-group">
                    <label for="post-title">Email Почта</label>
                    <input type="email" class="form-control" value="{{old('email')}}" name="email" id="email" placeholder="email">
                </div>
                <div class="form-group">
                    <label for="post-address">Адрес</label>
                    <textarea class="form-control"  name="address" id="post-address" rows="3" placeholder="Адрес Контакта">{{old('address')}}</textarea>
                </div>

                <div class="form-group">
                    <input type="file" name="image" class="form-control">
                </div>

                <div class="form-group">
                    <label for="post-price"  >Номер телефона</label>
                    <input type="text"  class="form-control" name="phone" id="phone" placeholder="номер телефона" value="{{old('phone')}}">
                </div>
                <button type="submit" class="btn btn-success">Добавить Контакт</button>
            </form>
        </div>
    </div>
@endsection
