@extends('layouts.app')

@section('title', $post->title)



@section('content')
    <div class="row">
        <div class="col-lg-6 mx-auto">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Error!</strong>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li></li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{route('posts.update',$post->id)}}" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label for="post-title">ФИО контакта</label>
                    <input type="text" class="form-control" value="{{$post->fio}}" name="fio" id="post-title" placeholder="ФИО контакта">
                </div>
                <div class="form-group">
                    <label for="post-title">email контакта</label>
                    <input type="text" class="form-control" value="{{$post->email}}" name="email" id="email" placeholder="email">
                </div>
                <div class="form-group">
                    <label for="post-address">Адрес контакта</label>
                    <textarea class="form-control"  name="address" id="post-address" rows="3" placeholder="Адрес контакта">{{$post->address}}</textarea>
                </div>

                <div class="form-group">
                    <input type="file" name="image" class="form-control" placeholder="Post Title">
                    <img src="{{ Storage::url($post->image) }}" height="200" width="200" alt="" />
                </div>


                <div class="form-group">
                    <label for="post-phone">Телефон контакта</label>
                    <input type="text"  class="form-control" name="phone" id="phone" placeholder="номер телефона" value="{{$post->phone}}">
                </div>
                <button type="submit" class="btn btn-success">Редактировать Контакт</button>
            </form>
        </div>
    </div>
@endsection
