@extends('layouts.app')

@section('fio',$post->fio)


@section('content')
    <div class="card">
        <div class="card-body">
            <h3>ФИО: {{$post->fio}}</h3>
            <h3>Почта :{{$post->email}}</h3>
            <p>Адрес: {{$post->description}}</p>
            <p><img src="{{ Storage::url($post->image) }}" height="75" width="75" alt="" /></p>
            <p>Номер телефона <b> {{$post->phone}}</b></p>
        </div>
    </div>
@endsection
