<?php $__env->startSection('title', 'Все посты'); ?>


<?php $__env->startSection('content'); ?>
    <a href="<?php echo e(route('posts.create')); ?>" class="btn btn-success">создать Контакт</a>
    <?php if(session()->get('success')): ?>
        <div class="alert alert-success mt-3">
            <?php echo e(session()->get('success')); ?>

        </div>
    <?php endif; ?>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">ФИО контакта</th>
            <th scope="col">Email</th>
            <th scope="col">Адрес</th>
            <th scope="col">Изображение</th>
            <th scope="col">Номер телефона</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <?php if($posts): ?>
        <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <th scope="row"><?php echo e($post->id); ?></th>
            <td><?php echo e($post->fio); ?></td>
            <td><?php echo e($post->email); ?></td>
            <td><?php echo e($post->address); ?></td>
            <td><img src="<?php echo e(Storage::url($post->image)); ?>" height="75" width="75" alt="" /></td>
            <td><?php echo e($post->phone); ?></td>
            <td class="table-buttons">
                <a href="<?php echo e(route('posts.show',$post)); ?>" class="btn btn-success"><i class="fa fa-eye"></i></a>
                <a href="<?php echo e(route('posts.edit',$post)); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                <form action="<?php echo e(route('posts.destroy',$post)); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <?php echo method_field('DELETE'); ?>
                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                </form>

            </td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php else: ?>
            <tr>
                <td colspan="3" class="text-danger">Контакты  не найдены.</td>
            </tr>
        <?php endif; ?>
        </tbody>

    </table>
    <div class="d-flex justify-content-center">
        <?php echo $posts->appends(['sort' => 'department'])->links(); ?>

    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OpenServerLast\domains\laravel7-crud\resources\views/posts/index.blade.php ENDPATH**/ ?>