<?php $__env->startSection('title', 'Добавить Пост'); ?>



<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-lg-6 mx-auto">
            <?php if($errors->any()): ?>
                <div class="alert alert-danger">
                    <strong>Error!</strong>
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>
            <form method="post" action="<?php echo e(route('posts.store')); ?>" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <div class="form-group">
                    <label for="post-title">ФИО</label>
                    <input type="text" class="form-control" value="<?php echo e(old('fio')); ?>" name="fio" id="post-fio" placeholder="ФИО">
                </div>
                <div class="form-group">
                    <label for="post-title">Email Почта</label>
                    <input type="email" class="form-control" value="<?php echo e(old('email')); ?>" name="email" id="email" placeholder="email">
                </div>
                <div class="form-group">
                    <label for="post-address">Адрес</label>
                    <textarea class="form-control"  name="address" id="post-address" rows="3" placeholder="Адрес Контакта"><?php echo e(old('address')); ?></textarea>
                </div>

                <div class="form-group">
                    <input type="file" name="image" class="form-control">
                </div>

                <div class="form-group">
                    <label for="post-price"  >Номер телефона</label>
                    <input type="text"  class="form-control" name="phone" id="phone" placeholder="номер телефона" value="<?php echo e(old('phone')); ?>">
                </div>
                <button type="submit" class="btn btn-success">Добавить Контакт</button>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OpenServerLast\domains\laravel7-crud\resources\views/posts/create.blade.php ENDPATH**/ ?>