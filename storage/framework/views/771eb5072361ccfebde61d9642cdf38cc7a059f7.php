<?php $__env->startSection('title', $post->title); ?>



<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-lg-6 mx-auto">
            <?php if($errors->any()): ?>
                <div class="alert alert-danger">
                    <strong>Error!</strong>
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>
            <form method="post" action="<?php echo e(route('posts.update',$post->id)); ?>" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <?php echo method_field('PATCH'); ?>
                <div class="form-group">
                    <label for="post-title">ФИО контакта</label>
                    <input type="text" class="form-control" value="<?php echo e($post->fio); ?>" name="fio" id="post-title" placeholder="ФИО контакта">
                </div>
                <div class="form-group">
                    <label for="post-title">email контакта</label>
                    <input type="text" class="form-control" value="<?php echo e($post->email); ?>" name="email" id="email" placeholder="email">
                </div>
                <div class="form-group">
                    <label for="post-address">Адрес контакта</label>
                    <textarea class="form-control"  name="address" id="post-address" rows="3" placeholder="Адрес контакта"><?php echo e($post->address); ?></textarea>
                </div>

                <div class="form-group">
                    <input type="file" name="image" class="form-control" placeholder="Post Title">
                    <img src="<?php echo e(Storage::url($post->image)); ?>" height="200" width="200" alt="" />
                </div>


                <div class="form-group">
                    <label for="post-phone">Телефон контакта</label>
                    <input type="text"  class="form-control" name="phone" id="phone" placeholder="номер телефона" value="<?php echo e($post->phone); ?>">
                </div>
                <button type="submit" class="btn btn-success">Редактировать Контакт</button>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OpenServerLast\domains\laravel7-crud\resources\views/posts/edit.blade.php ENDPATH**/ ?>