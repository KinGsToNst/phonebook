<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', function () {
    return redirect('/posts');
});
Route::get('/', 'PostController@index');
Route::resource('posts','PostController');

/*Поиск записей*/
Route::get('/search','PostController@search');
/*Поиск записей*/
/*контактная форма*/
Route::get('ajax-contact-form', 'AjaxContactController@index');
Route::post('save', 'AjaxContactController@store');
/*контактная форма*/
/*pagination*/

/*pagination*/
