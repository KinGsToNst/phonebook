<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //$posts=Post::all();
        //dd($posts);
        $posts = Post::paginate(3);
        return view('posts.index',compact('posts'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'fio' => 'required',
            'email' => 'required',
            'address' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            'phone' => 'required'
        ]);
        $path = $request->file('image')->store('public/images');
        $post=new Post([
           'fio'=>$request->get('fio'),
           'email'=>$request->get('email'),
           'address'=>$request ->get('address'),
           'image'=>$path,
           'phone'=>$request ->get('phone'),
        ]);

        $post->save();
        return redirect('/posts')->with('success', 'Пост успешно добавлен.');
    }

    /**h
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post=Post::find($id);
        return view('posts.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post=Post::find($id);
        return view('posts.edit',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            'fio' => 'required',
            'email' => 'required',
            'address' => 'required',
            'phone' => 'required'
        ]);




        $post = Post::find($id);
        $post->fio =  $request->get('fio');
        $post->email =  $request->get('email');
        $post->address =  $request->get('address');

        if($request->hasFile('image')){
            $request->validate([
                'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            $path = $request->file('image')->store('public/images');
            $post->image = $path;
        }

        $post->phone =  $request->get('phone');
        $post->save();
        return redirect('/posts')->with('success', 'Пост успешно обновлен.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post=Post::find($id);
        $post->delete();
        return redirect('/posts')->with('success', 'Пост успешно удален.');
    }

    public function search(){
        $search_text=$_GET['query'];
        $posts=Post::where('fio','LIKE','%'.$search_text.'%')->orWhere('phone', 'LIKE', '%'.$search_text.'%')->orWhere('address', 'LIKE', '%'.$search_text.'%')->get();
        return view('posts.search',compact('posts'));
    }




}
